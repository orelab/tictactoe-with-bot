import React from 'react';

class Case extends React.Component {

    render() {
        return (
            <button onClick={() => this.props.onClick()}>
                {this.props.value} &nbsp;
            </button>
        )
    }
}

export default Case;