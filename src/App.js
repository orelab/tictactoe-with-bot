import Case from './Case';
import './App.css';
import React from 'react';


class App extends React.Component {

  winList = [
    [0, 1, 2], [3, 4, 5], [6, 7, 8], // horizontal
    [0, 3, 6], [1, 4, 7], [2, 5, 8], // vertical
    [0, 4, 8], [2, 4, 6]             // diagonal
  ];

  constructor(props) {
    super(props);
    this.state = {
      sign: false,
      cross: Array(9).fill('')
    };
  }

  /*
    The player turn
  */
  doPlayerUpdate = function (i) {
    if (this.state.cross[i] !== '') {
      alert('Déjà pris !');
      return;
    }
    let cross = this.state.cross.slice()
    cross[i] = this.state.sign ? 'X' : 'O';

    this.setState({
      sign: !this.state.sign,
      cross: cross
    }, () => {
      this.doBotUpdate();
    });

  }

  /*
    The bot turn
    If a winning triplet (ex: [0,1,2]) is filled with 2 same values 
    and an empty value, the bot will play that last value.
    If not, the bot play a random empty value !
  */
  doBotUpdate = function () {
    for (let i = 0; i < this.winList.length; i++) { // for, a breakable loop...
      let triple = this.winList[i];

      let cross = this.state.cross
        .slice()
        .filter((val, i) => {
          return triple.includes(i);
        })

      let nX = cross.filter(a => a === 'X').length;
      let nO = cross.filter(a => a === 'O').length;
      let nE = cross.filter(a => a === '').length;

      // retrieve the empty place to play
      let n = this.state.cross.findIndex((el, i) => {
        return el === '' && triple.includes(i);
      });

      if (nE === 1 && (nX === 2 || nO === 2)) { // 1 empty + 2 X or 0
        // Simulate bot's click
        cross = this.state.cross.slice()
        cross[n] = this.state.sign ? 'X' : 'O';
        this.setState({
          sign: !this.state.sign,
          cross: cross
        });

        console.log('Bot wisely played ' + nE);
        return; // The bot found its play, stop looping !
      }
    }

    // If the program is here, it means that no important play
    // has been found, so we choose an empty play, randomly
    let rnd = this.state.cross
      .slice()
      .map((val, i) => val === '' ? i : '')
      .filter(val => val !== '')
    let p = rnd[Math.floor((Math.random() * rnd.length))];

    let cross = this.state.cross.slice()
    cross[p] = this.state.sign ? 'X' : 'O';
    this.setState({
      sign: !this.state.sign,
      cross: cross
    });
    console.log('Bot randomly played ' + p);

  }

  /*
    Define the winner... or draw game !
  */
  componentDidUpdate = function () {
    this.winList.map(c => {
      let cross = this.state.cross;
      if (cross[c[0]] !== ''
        && cross[c[0]] === cross[c[1]]
        && cross[c[1]] === cross[c[2]]) {
        alert('"' + cross[c[0]] + '" a gagné !');
        this.setState({ cross: Array(9).fill('') })
        return;
      }
    });

    if (!this.state.cross.includes('')) {
      alert('Match nul !');
      this.setState({ cross: Array(9).fill('') })
      return;
    }
  }

  render() {

    console.log(this.state.cross);

    return (
      <div className="App">
        <Case onClick={() => this.doPlayerUpdate(0)} value={this.state.cross[0]}></Case>
        <Case onClick={() => this.doPlayerUpdate(1)} value={this.state.cross[1]}></Case>
        <Case onClick={() => this.doPlayerUpdate(2)} value={this.state.cross[2]}></Case>
        <Case onClick={() => this.doPlayerUpdate(3)} value={this.state.cross[3]}></Case>
        <Case onClick={() => this.doPlayerUpdate(4)} value={this.state.cross[4]}></Case>
        <Case onClick={() => this.doPlayerUpdate(5)} value={this.state.cross[5]}></Case>
        <Case onClick={() => this.doPlayerUpdate(6)} value={this.state.cross[6]}></Case>
        <Case onClick={() => this.doPlayerUpdate(7)} value={this.state.cross[7]}></Case>
        <Case onClick={() => this.doPlayerUpdate(8)} value={this.state.cross[8]}></Case>
      </div>
    );
  }
}

export default App;
